const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  productionSourceMap: false,
  chainWebpack: config => {
    config.when(process.env.NODE_ENV === 'production', config => {
      const cdn = {
        js: [
          'https://cdn.staticfile.org/axios/1.2.1/axios.min.js',
          'https://cdn.staticfile.org/vue/2.6.14/vue.min.js',
          'https://cdn.staticfile.org/vue-router/3.5.1/vue-router.min.js',
          'https://cdn.staticfile.org/element-ui/2.15.12/index.js',
          'https://cdn.bootcss.com/vuex/3.6.2/vuex.min.js'
        ],
        css: [
          'https://cdn.staticfile.org/element-ui/2.15.12/theme-chalk/index.css'
        ]
      }


      config.set('externals', {
        //key是包，value是对象
        vue: 'Vue',
        vuex: "Vuex",
        'axios': 'axios',
        'vue-router': 'VueRouter',
        './plugins/index.js': 'ELEMENT',
      })

      config.plugin('html').tap(args => {
        args[0].isProd = true
        args[0].cdn = cdn
        return args
      })
    })
  }
})
