import axios from 'axios';
const service = axios.create({
    // baseURL: 'http://localhost:3000',
    timeout: 5000
});

service.interceptors.request.use(
    config => {
        if (store.state.token) {
            config.headers.Authorization = store.state.token
        }
        return config;
    },
    error => {
        return Promise.reject(error);
    }
);

service.interceptors.response.use(
    response => {
        if (response.status === 200) {
            return response.data;
        } else {
            Promise.reject();
        }
    },
    error => {
        return Promise.reject(error)
    }
);