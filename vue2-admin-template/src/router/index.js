import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/kindergarten'
  }, {
    path: '/home',
    name: 'Home',
    component: Home,
    children: [
      {
        path: '/kindergarten',
        name: 'kindergarten',
        meta: {
          title: '幼儿园'
        },
        component: () => import( /* webpackChunkName: "kindergarten" */ "../views/school/kindergarten.vue")
      },
      {
        path: '/primary',
        name: 'primary',
        meta: {
          title: '小学'
        },
        component: () => import( /* webpackChunkName: "primary" */ "../views/school/primary.vue")
      },
      {
        path: '/junior',
        name: 'junior',
        meta: {
          title: '初中'
        },
        component: () => import( /* webpackChunkName: "junior" */ "../views/school/junior.vue")
      },
      {
        path: '/senior',
        name: 'senior',
        meta: {
          title: '高中'
        },
        component: () => import( /* webpackChunkName: "senior" */ "../views/school/senior.vue")
      },
      {
        path: '/university',
        name: 'university',
        meta: {
          title: '大学'
        },
        component: () => import( /* webpackChunkName: "university" */ "../views/school/university.vue")
      }
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
