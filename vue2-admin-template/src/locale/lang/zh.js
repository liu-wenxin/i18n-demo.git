export default {
    zh: {
        title: '后台管理系统',
        schoolModule: {
            title: '学校模块',
            kindergarten: '幼儿园',
            primary: '小学',
            junior: '初中',
            senior: '高中',
            university: '大学'
        }
    }
}