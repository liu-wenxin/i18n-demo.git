import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
export default defineConfig({
  productionSourceMap: false,
  plugins: [
    vue()
  ]
})
