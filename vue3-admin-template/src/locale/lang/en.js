export default {
    en: {
        title: 'management system',
        schoolModule: {
            title: 'schoolModule',
            kindergarten: 'kindergarten',
            primary: 'primary',
            junior: 'junior',
            senior: 'senior',
            university: 'university'
        }
    }
}
