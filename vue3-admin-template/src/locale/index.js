import { createI18n } from 'vue-i18n'

import messages from './lang'
const i18n = createI18n({
    legacy: false, //处理报错Uncaught (in promise) SyntaxError: Not available in legacy mode (at message-compiler.esm-bundler.js:54:19)
    locale: 'zh',
    messages
})

export default (app) => {
    app.use(i18n)
}
