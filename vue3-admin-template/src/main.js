import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import lang from './locale'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
const app = createApp(App)
lang(app)
app.use(store)
    .use(ElementPlus)
    .use(router)
    .mount('#app')
